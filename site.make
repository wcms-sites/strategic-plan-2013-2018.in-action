core = 7.x
api = 2

; UWaterloo Strategic Plan
projects[uw_strategic_plan][type] = "module"
projects[uw_strategic_plan][download][type] = "git"
projects[uw_strategic_plan][download][url] = "https://git.uwaterloo.ca/wcms/uw_strategic_plan.git"
projects[uw_strategic_plan][download][tag] = "7.x-1.2"

; UWaterloo Strategic Plan theme
projects[uw_theme_strategic_plan][type] = "theme"
projects[uw_theme_strategic_plan][download][type] = "git"
projects[uw_theme_strategic_plan][download][url] = "https://git.uwaterloo.ca/wcms/uw_theme_strategic_plan.git"
projects[uw_theme_strategic_plan][download][tag] = "7.x-1.2"
